﻿/**
 * Get function's parameter names.
 *
 * @param  {function}  fn     A function.
 * @returns {array<strings>}  An array of function's parameter names.
 */
const getParams = function(fn) {
    'use strict';

    //! 1) get fn source with toString (toSource doesnt work in Node)
    let src = fn.toString().trim();

    //! 2) remove comments
    const COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    src = src.replace(COMMENTS, '').trim();

    //! 3) extract content between parenthesis
    const EXTRACTPARENS = /^(?:(?:function.*\(([^]*?)\))|(?:([^\(\)]+?)\s*=>)|(?:\(([^]*?)\)\s*=>))[^]+$/gm;
    src = src.replace(EXTRACTPARENS, "$1$2$3");

    //! 4) remove whitespace
    const NL = /[\s]/mg;
    src = src.replace(NL, "").trim();

    //! DESTRUCTURING?
    const DESTR = /(:?[\[\{])([^]*?)(:?[\]\}])(=\1(.*?)?\3)?/gm;
    if (DESTR.test(src)) {
        const RHS = /(:?[\[\{])(.*?)(:?[\]\}])(=\1.*?\3)?/gm;
        src = RHS.exec(src)[2];
    }

    //! 5) SPLIT STRING INTO ARRAY
    const DELSPLIT = /\s*,\s*/;
    src = src.split(DELSPLIT);

    //! CLEAN ARRAY ELEMENTS: a) remove SPREAD operator
    const DELSP = /\.{3}/;
    src = src.map(el => el.replace(DELSP, "").trim());

    //! CLEAN ARRAY ELEMENTS: b) remove DEFAULT values
    const DELDEF = /[=\s].*$/g;
    src = src.map(el => el.replace(DELDEF, "").trim());

    //! CLEAN ARRAY ELEMENTS: c) remove ALIASED param
    const DELALIAS = /^.*[:\s]/g;
    src = src.map((v) => v.replace(DELALIAS, "").trim());

    return src;
}


if (typeof module !== "undefined") module.exports = getParams;

