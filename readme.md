# `getParams()`

`getParams` - Get names of formal parameters of a function.    
    

## DESCRIPTION
This utility will extract the names of function's formal parameters and return them as a string array. This utility does not have any issues with the form of a function (whether it is a function declaration, function expression or arrow function) nor with embedded comments, newlines and whitespace; it will correctly deal with parameters that use default values and the spread operator (usualy, the arity of a function is obtained through function's `length` property, but only in case that no parameter use default values or spread operator), so it is easy to obtain function's arity.    
    
This utility will also try to extract parameters even when destructuring is used. This should work for some, but certainly not for all imaginable forms of parameter destructuring, so use with caution.    
    

## EXAMPLES

```js
function f(a, b = 2, ...c) {}
//=> [a, b, c]
const f = a => {};
//=> [a]

function f ({x:a=5, b, z:c}) {};
//=> [a, b, c]
function f({x:a=6, y:b=9, c=6} = {}) {};
//=> [a, b, c]
function f({ x:a, y:b=5, z:c=4 } = {x, y, z}) {};
//=> [a, b, c]
let f = ({x:a=5, y:b=6, z:c } = {x:1, y:2, z:3}) => {};
//=> [a, b, c]
function f([a=5, b=6, ...c] = [3, 4, 5]) {};
//=> [a, b, c]
```


*DESCRIPTION*    
    `getParams` - Get names of individual formal parameters of a function.    
    
*SYNOPSIS:*    
    `getParams(fx)`    
    
*PARAMETERS:*    
    `fx` A function.    
    
*RETURN VALUE:*    
    An array of strings containing names of function's parameters.    
    


## INSTALL

[npm](https://www.npmjs.com/)    
    
```shell
$ npm install @jsbx/get-params
```


## USAGE    

```js
var getParams = require('@jsbx/get-params');

function f(a=5, b=2, ...c) {}
getParams(f); //=> [a, b, c]
```
